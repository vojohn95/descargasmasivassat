<?php

namespace App\Http\Controllers;

use Exception;
use Throwable;
use DateTimeImmutable;
use App\Imports\UUIDImport;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use PhpCfdi\Credentials\Credential;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Filesystem\Filesystem;
use PhpCfdi\CfdiSatScraper\SatScraper;
use PhpCfdi\CfdiSatScraper\ResourceType;
use PhpCfdi\CfdiSatScraper\Filters\DownloadType;
use PhpCfdi\CfdiSatScraper\Contracts\MaximumRecordsHandler;
use PhpCfdi\CfdiSatScraper\Sessions\Fiel\FielSessionManager;


class FacturaController extends Controller
{
    public function store(Request $request)
    {
        $data = Excel::toArray(new UUIDImport, $request->excel);
        $UUID = Arr::pluck($data[1], '0');
        $result = Arr::except($UUID, 0);

        $privateKey = public_path('doc/Claveprivada_FIEL_OCE9412073L3_20201005_145545.key');
        $certificate = public_path('doc/00001000000505286952.cer');
        $passPhrase = 'BAFI0510OCE20';
        $path = public_path('downloads');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
        // crear la credencial
        // se puede usar Credential::openFiles(certificateFile, privateKeyFile, passphrase) si la FIEL está en archivos
        $credential = Credential::openFiles($certificate, $privateKey, $passPhrase);
        if (!$credential->isFiel()) {
            throw new Exception('The certificate and private key is not a FIEL');
        }
        if (!$credential->certificate()->validOn()) {
            throw new Exception('The certificate and private key is not valid at this moment');
        }

        $handler = new class() implements MaximumRecordsHandler
        {
            public function handle(DateTimeImmutable $date): void
            {
                echo 'Se encontraron más de 500 CFDI en el segundo: ', $date->format('c'), PHP_EOL;
            }
        };

        // crear el objeto scraper usando la FIEL
        $satScraper = new SatScraper(FielSessionManager::create($credential));

        try {
            if ($request->tipo == 'emitidos') {
                $list = $satScraper->listByUuids($result, DownloadType::emitidos());
            } else {
                $list = $satScraper->listByUuids($result, DownloadType::recibidos());
            }

            // $downloadedUuids contiene un listado de UUID que fueron procesados correctamente, 50 descargas simultáneas
            $downloadedUuids = $satScraper->resourceDownloader(ResourceType::xml(), $list, 50)
                ->saveTo($path, true, 0777);
            $downloadedUuidspdfs = $satScraper->resourceDownloader(ResourceType::pdf(), $list, 50)
                ->saveTo($path, true, 0777);

            $zip_file = 'facturas.zip';
            $zip = new \ZipArchive();
            $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

            $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
            foreach ($files as $name => $file) {
                // We're skipping all subfolders
                if (!$file->isDir()) {
                    $filePath  = $file->getRealPath();

                    // extracting filename with substr/strlen
                    $relativePath = 'invoices/' . substr($filePath, strlen($path) + 1);

                    $zip->addFile($filePath, $relativePath);
                }
            }
            $zip->close();
            $limpieza = new Filesystem;
            $limpieza->cleanDirectory($path);
            return response()->download($zip_file);
        } catch (Throwable $e) {
            dd($e);
        }
    }
}
